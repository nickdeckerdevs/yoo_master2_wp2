<?php 
/**
 * List View Single Event
 * This file contains one event in the list view
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/list/single-event.php
 *
 * @package TribeEventsCalendar
 * @since  3.0
 * @author Modern Tribe Inc.
 *
 */

if ( !defined('ABSPATH') ) { die('-1'); } ?>

<?php 


$postid = get_the_ID();
global $wpdb;
$pre = $wpdb->prefix;
$sql = 'SELECT * FROM ' . $pre . 'imc_venue INNER JOIN ' . $pre . 'imc_events ON ' . $pre . 'imc_venue.id = ' . $pre . 'imc_events.venue_id WHERE event_id = ' . $postid;
$venue_details_imc = $wpdb->get_row( $sql );

?>
<!-- Event Cost -->
<?php if ( tribe_get_cost() ) : ?> 
	<div class="tribe-events-event-cost">
		<span><?php echo tribe_get_cost( null, true ); ?></span>
	</div>
<?php endif; ?>

<!-- Event Title -->
<?php do_action( 'tribe_events_before_the_event_title' ) ?>
<h2 class="tribe-events-list-event-title entry-title summary">
	<a class="url" href="<?php echo tribe_get_event_link() ?>" title="<?php the_title() ?>" rel="bookmark">
            <?php 
            
            $title = the_title( '', '', false ); 
            echo ucwords( $title ); ?>
        </a>
</h2>
<?php do_action( 'tribe_events_after_the_event_title' ) ?>

<!-- Event Meta -->
<?php do_action( 'tribe_events_before_the_meta' ) ?>
<div class="tribe-events-event-meta vcard"> <div class="author">

	<!-- Schedule & Recurrence Details -->
	<div class="updated published time-details">
		<?php echo tribe_events_event_schedule_details() ?>
	</div>

	
		<!-- Venue Display Info -->
		<div class="tribe-events-venue-details">
                <?php 
                if( $venue_details_imc != null ) {
                    $venue_link = '<a href="' . get_site_url() . $venue_details_imc->wp_guid . '">' . addslashes( ucwords( $venue_details_imc->name ) ) . '</a>';
                    echo $venue_link;                    
                } ?>
                    
		</div> <!-- .tribe-events-venue-details -->
	

</div> </div><!-- .tribe-events-event-meta -->
<?php do_action( 'tribe_events_after_the_meta' ) ?>

<!-- Event Image -->
<?php echo tribe_event_featured_image( null, 'medium' ) ?>

<!-- Event Content -->
<?php do_action( 'tribe_events_before_the_content' ) ?>
<div class="tribe-events-list-event-description tribe-events-content description entry-summary">
	<?php the_excerpt() ?>
	<a href="<?php echo tribe_get_event_link() ?>" class="tribe-events-read-more" rel="bookmark"><?php _e( 'Find out more', 'tribe-events-calendar' ) ?> &raquo;</a>
</div><!-- .tribe-events-list-event-description -->
<script>
jQuery(document).ready(function($) {
   $('ul.uk-navbar-nav.uk-hidden-small>li:eq(1)').addClass('uk-active');     
});
</script>
    <?php do_action( 'tribe_events_after_the_content' );
