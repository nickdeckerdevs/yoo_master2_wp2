<?php
/**
 * Single Event Template
 * A single event. This displays the event title, description, meta, and
 * optionally, the Google map for the event.
 * 
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/single-event.php
 *
 * @package TribeEventsCalendar
 * @since  2.1
 * @author Modern Tribe Inc.
 *
 */

if ( !defined('ABSPATH') ) { die('-1'); }

$event_id = get_the_ID();

?>

<div id="tribe-events-content" class="tribe-events-single">

	<p class="tribe-events-back"><a href="<?php echo tribe_get_events_link() ?>"> <?php _e( '&laquo; All Events', 'tribe-events-calendar' ) ?></a></p>

	<!-- Notices -->
	<?php tribe_events_the_notices() ?>
        <?php the_title( '<h2 class="tribe-events-single-event-title summary">', '</h2>' ); ?>
        <?php //$name = get_query_var( 'name' );
        //$array = explode( '-', $name );
        //imc_get_event_venue_id( get_query_var );
        
        //echo 'postname: ' . $name . ' - array0' . $array[0]; 
        $venue = imc_get_venue_details_event( imc_get_event_venue_id( get_query_var( 'name' ) ) );
        //var_dump( $venue);
        $street = trim( $venue->address . ' ' . $venue->address2 ); 
        $address = $street . ' ' . $venue->city; 
        $map_url = 'http://maps.google.com/?q=' . $address . ' ' . $venue->state . ' ' . $venue->zip; ?>
        <h3>Address: <a href="<?php echo get_permalink( $venue->wp_post_id ); ?>"><?php echo $venue->name; ?></a> | <a href="<?php echo $map_url ?>" target="_blank"><?php echo $address; ?></a></h3>
        
	<div class="tribe-events-schedule updated published tribe-clearfix">
		<?php echo tribe_events_event_schedule_details( $event_id, '<h3>', '</h3>'); ?>
		<?php  if ( tribe_get_cost() ) :  ?>
			<span class="tribe-events-divider">|</span>
			<span class="tribe-events-cost"><?php echo tribe_get_cost( null, true ) ?></span>
		<?php endif; ?>
	</div>

	<!-- Event header -->
	<div id="tribe-events-header" <?php tribe_events_the_header_attributes() ?>>
		<!-- Navigation -->
		<h3 class="tribe-events-visuallyhidden"><?php _e( 'Event Navigation', 'tribe-events-calendar' ) ?></h3>
		<ul class="tribe-events-sub-nav">
			<li class="tribe-events-nav-previous"><?php tribe_the_prev_event_link( '<span>&laquo;</span> %title%' ) ?></li>
			<li class="tribe-events-nav-next"><?php tribe_the_next_event_link( '%title% <span>&raquo;</span>' ) ?></li>
		</ul><!-- .tribe-events-sub-nav -->
	</div><!-- #tribe-events-header -->
        <div id="sponsored-event" style="display: none;"><a href="http://chooseimpulse.com" target="_blank"><img src="http://www.lorempixum.com/g/350/275/sports" alt="" /></a></div>
	<?php while ( have_posts() ) :  the_post(); ?>
		<div id="post-<?php the_ID(); ?>" <?php post_class('vevent'); ?>>
			<!-- Event featured image -->
			<?php echo tribe_event_featured_image(); ?>

			<!-- Event content -->
			<?php do_action( 'tribe_events_single_event_before_the_content' ) ?>
			<div class="tribe-events-single-event-description tribe-events-content entry-content description">
				<?php the_content(); ?>
			</div><!-- .tribe-events-single-event-description -->
			<?php do_action( 'tribe_events_single_event_after_the_content' ) ?>
                        
			<!-- Event meta -->
			<?php do_action( 'tribe_events_single_event_before_the_meta' ) ?>
                        
				<?php echo tribe_events_single_event_meta() ?>
                        <div id="map-that-moves">
                            <a href="<?php echo $map_url; ?>" target="_blank">
                                <img class="map-image" src="http://maps.googleapis.com/maps/api/staticmap?center=<?php echo $address; ?>&amp;zoom=15&amp;size=450x250&amp;maptype=roadmap&amp;markers=color:red%7Clabel<?php echo $venue->name; ?>%7C<?php echo $address; ?>">
                            </a>
                        </div>
                        
			<?php do_action( 'tribe_events_single_event_after_the_meta' ) ?>
		
                </div><!-- .hentry .vevent -->
		<?php if( get_post_type() == TribeEvents::POSTTYPE && tribe_get_option( 'showComments', false ) ) comments_template() ?>
	<?php endwhile; ?>
                        
	<!-- Event footer -->
    <div id="tribe-events-footer">
		<!-- Navigation -->
		<!-- Navigation -->
		<h3 class="tribe-events-visuallyhidden"><?php _e( 'Event Navigation', 'tribe-events-calendar' ) ?></h3>
		<ul class="tribe-events-sub-nav">
			<li class="tribe-events-nav-previous"><?php tribe_the_prev_event_link( '<span>&laquo;</span> %title%' ) ?></li>
			<li class="tribe-events-nav-next"><?php tribe_the_next_event_link( '%title% <span>&raquo;</span>' ) ?></li>
		</ul><!-- .tribe-events-sub-nav -->
	</div><!-- #tribe-events-footer -->

</div><!-- #tribe-events-content -->

<script>
jQuery(document).ready(function($) {
   $('ul.uk-navbar-nav.uk-hidden-small>li:eq(1)').addClass('uk-active');     
   var map = $('#map-that-moves').html();
   $('.tribe-events-meta-group.tribe-events-meta-group-details').append('<div id="event-map">' + map + '</div>');
   $('#map-that-moves').remove();
});
</script>
<a class="create-event-button" href="<?php echo get_site_url(); ?>/events/create-event">Add An Event</a>