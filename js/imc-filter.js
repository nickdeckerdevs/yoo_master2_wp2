/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function show_slide_down($target) {
    $target.parent().find('.filter-options').slideDown();
    $target.addClass('expand').removeClass('normal');
}

function show_slide_up($target) {
    $target.parent().find('.filter-options').slideUp();
    $target.addClass('normal').removeClass('expand');
}

jQuery(document).ready(function($) {
    $('.filter').on('click', 'h2.normal', function() {
        show_slide_down($(this));
    });
    
    $('.filter').on('click', 'h2.expand', function() {
        show_slide_up($(this));        
    });
    $('.filter-options').on('click', 'label', function() {
        //console.log($(this));
    });
    
});

