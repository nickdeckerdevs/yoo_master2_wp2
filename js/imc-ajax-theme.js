/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

jQuery(document).ready(function($) {
    $('#imc-filter-venues').on('click', 'input', function(e) {
        get_new_results(e);
    });
    $('#imc-filter-venues').on('change', 'select', function(e) {
        get_new_results(e);
    });
    function get_new_results(e) {
    e.stopPropagation();
    data = $('#imc-filter-venues').serialize();
    $.post(ajaxurl, data, function (response) {
        $('.tm-content').html(response);
        $('.die').html('Change your filter options to see more results >>');
    });
    
        
}
});