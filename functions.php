<?php
/**
* @package   yoo_master2
* @author    YOOtheme http://www.yootheme.com
* @copyright Copyright (C) YOOtheme GmbH
* @license   http://www.gnu.org/licenses/gpl.html GNU/GPL
*/

// check compatibility
if (version_compare(PHP_VERSION, '5.3', '>=')) {

    // bootstrap warp
    require(__DIR__.'/warp.php');
}

add_action( 'wp_enqueue_scripts', 'imc_scripts_theme' );

function imc_scripts_theme() {
    wp_enqueue_script ( 'imc-filter', get_template_directory_uri().'/js/imc-filter.js', array( 'jquery' ) ); 
    wp_enqueue_script ( 'imc-ajax-theme', get_template_directory_uri() . '/js/imc-ajax-theme.js', array( 'jquery' ) );
    
}

function add_ajaxurl_cdata_to_front(){ ?>
    <script type="text/javascript"> //<![CDATA[
        ajaxurl = '<?php echo admin_url( 'admin-ajax.php'); ?>';
    //]]> </script>
<?php }
add_action( 'wp_head', 'add_ajaxurl_cdata_to_front', 1);
add_action( 'wp_ajax_imc_filter_venues', 'imc_filter_venues' );
add_action( 'wp_ajax_nopriv_imc_filter_venues', 'imc_filter_venues' );
add_action( 'wp_ajax_imc_filter_hh', 'imc_filter_hh' );
add_action( 'wp_ajax_nopriv_imc_filter_hh', 'imc_filter_hh' );